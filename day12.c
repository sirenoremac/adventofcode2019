//
//  main.c
//  AdventOfCode Day12
//
//  Created by Eric Cameron on 12/11/19.
//  Copyright © 2019. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int moons_test[24] =    {-1,2,4,3,0,-10,-8,5,2,-7,8,-1,0,0,0,0,0,0,0,0,0,0,0,0};

int moons_test_2[24] =    {-8,5,2,9,-10,5,-7,-8,0,10,3,-3,0,0,0,0,0,0,0,0,0,0,0,0};

//my input data
int moons[24] =    {14, 12, 1, 16, 4, 10, 7, -5, 5, 8, -10, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

int pairs[6][2] = {{0, 1},
    {0, 2},
    {0, 3},
    {1, 2},
    {1, 3},
    {2, 3}};

void step(int* moons)
{
    for(int i = 0; i < 6; i++)
    {

        int moon = pairs[i][0];
        int other = pairs[i][1];
        if(moons[moon] > moons[other])
        {
            moons[12+moon] -= 1;
            moons[12+other] += 1;
        }
        else if(moons[moon] < moons[other])
        {
            moons[12+moon] += 1;
            moons[12+other] -= 1;
        }

        if(moons[4+moon] > moons[4+other])
        {
            moons[16+moon] -= 1;
            moons[16+other] += 1;
        }
        else if(moons[4+moon] < moons[4+other])
        {
            moons[16+moon] += 1;
            moons[16+other] -= 1;
        }

        if(moons[8+moon] > moons[8+other])
        {
            moons[20+moon] -= 1;
            moons[20+other] += 1;
        }
        else if( moons[8+moon] < moons[8+other])
        {
            moons[20+moon] += 1;
            moons[20+other] -= 1;
        }
    }
    for(int i = 0; i < 4; i++)
    {
        moons[i] += moons[12+i];
        moons[4+i] += moons[16+i];
        moons[8+i] += moons[20+i];
    }
}

uint64_t find_cycle(int* moons_og, int axis)
{
    int moons[24];
    for(int i = 0; i < 24; i++)
    {
        moons[i] = moons_og[i];
    }

    uint64_t cycle_steps = 0;
    int og_p1 = moons_og[axis*4];
    int og_p2 = moons_og[axis*4+1];
    int og_p3 = moons_og[axis*4+2];
    int og_p4 = moons_og[axis*4+3];
    while(1)
    {
        step(moons);
        cycle_steps++;
        
        if(moons[axis*4+0] == og_p1 && moons[axis*4+12+0] == 0 &&
           moons[axis*4+1] == og_p2 && moons[axis*4+12+1] == 0 &&
           moons[axis*4+2] == og_p3 && moons[axis*4+12+2] == 0 &&
           moons[axis*4+3] == og_p4 && moons[axis*4+12+3] == 0)
        {
            return cycle_steps;
        }
    }
}

uint64_t gcd(uint64_t a, uint64_t b)
{
    if (b == 0)
        return a;
    return gcd(b, a % b);
}


uint64_t find_lcm(uint64_t arr[], int len)
{
    uint64_t res = arr[0];
    
    for (int i = 1; i < len; i++)
    {
        uint64_t thegcd = gcd(arr[i], res);
        res = (arr[i] * res) / thegcd;
    }
    
    return res;
}

int main(int argc, const char * argv[])
{
    uint64_t cyclex = find_cycle(moons_test_2,0);
    uint64_t cycley = find_cycle(moons_test_2,1);
    uint64_t cyclez = find_cycle(moons_test_2,2);
    uint64_t cycles[3] = {cyclex,cycley,cyclez};
    uint64_t lcm = find_lcm(cycles, 3);
    printf("Repeats in %lld cycles\n",lcm);
    
    return 0;
}
